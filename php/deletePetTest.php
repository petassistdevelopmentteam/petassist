<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("DELETE FROM PetTest WHERE pet_id = ? AND scientific_name = ?");
$stmt->execute([$_SESSION['pets'][$_POST['petName']], $_POST['diseaseName']]);

//send back the number of records deleted
$status = $stmt->rowCount() > 0;

//send back a JSON
echo json_encode(array('status' => $status));
?>