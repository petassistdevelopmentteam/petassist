<?php
session_start();
require 'connect.php';

function validateDate($date, $format = 'Y/m/d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

if(isset($_POST['addPupper'])){
	$name = $_POST['name'];
	$breed = $_POST['breed'];
	$age = $_POST['age'];	
	$birthday = $_POST['birthday'];
	if(validateDate($birthday, 'Y/m/d')){
		$stmt = $pdo->prepare("SELECT owner_id FROM OwnerLogin WHERE username = ?");
		$stmt->execute([$_SESSION['username']]);
		$id = $stmt->fetchColumn();
		
		$stmt = $pdo->prepare("INSERT INTO PetInformation(owner_id, pet_name, breed_id, pet_age, pet_birthday) VALUES (?,?,?,?,?)");
		$stmt->execute([$id, $name, $breed, $age, $birthday]);

		header("Location: ../user_account.php");
		exit();
	}
	else{
		header("Location: ../add_pupper.php?error=x234");
		exit();
	}
	
}
?>