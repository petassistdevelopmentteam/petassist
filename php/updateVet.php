<?php
session_start();
require 'connect.php';

if(isset($_POST['addVet'])){
    $name = $_POST['name'];
    $oldName = $_POST['oldName'];
    $phone = $_POST['phone'];
    $fax = $_POST['fax'];	
    $address = $_POST['address'];

    $stmt = $pdo->prepare("UPDATE Veterinarian SET vet_name = ?, vet_phone = ?, vet_fax = ?, vet_address = ? WHERE vet_id = ?");
    $stmt->execute([$name, $phone, $fax, $address, $_SESSION['vets'][$oldName]]);

    $_SESSION['vets'][$name] = $_SESSION['vets'][$oldName];
    unset($_SESSION['vets'][$oldName]);

    header("Location: ../vet_page.php?name=".$name);
    exit();
}
?>