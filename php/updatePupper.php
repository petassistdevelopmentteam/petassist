<?php
session_start();
require 'connect.php';

function validateDate($date, $format = 'Y/m/d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

if(isset($_POST['addPupper'])){
    $name = $_POST['name'];
    $oldName = $_POST['oldName'];
	$breed = $_POST['breed'];
	$age = $_POST['age'];	
	$birthday = $_POST['birthday'];
	
	if(!empty($_POST['birthday'])){
		if(validateDate($birthday, 'Y/m/d')){
			$stmt = $pdo->prepare("UPDATE PetInformation SET pet_birthday = ? WHERE pet_id = ?");
			$stmt->execute([$birthday, $_SESSION['pets'][$oldName]]);
		}
		else{
			//invalid birthday format, redirect to pupper page with error
			header("Location: ../pupper_page.php?name=".$oldName."&error=x234");
			exit();
		}
	}
	if(!empty($_POST['breed'])){
		$stmt = $pdo->prepare("UPDATE PetInformation SET breed_id = ? WHERE pet_id = ?");
		$stmt->execute([$breed, $_SESSION['pets'][$oldName]]);
	}
	if(!empty($_POST['age'])){
		$stmt = $pdo->prepare("UPDATE PetInformation SET pet_age = ? WHERE pet_id = ?");
		$stmt->execute([$age, $_SESSION['pets'][$oldName]]);
	}
	if(!empty($_POST['name'])){
		$stmt = $pdo->prepare("UPDATE PetInformation SET pet_name = ? WHERE pet_id = ?");
		$stmt->execute([$name, $_SESSION['pets'][$oldName]]);
		
		//add new pet name to pets array with the same value as $oldName
		//unset oldName
		$_SESSION['pets'][$name] = $_SESSION['pets'][$oldName];
		unset($_SESSION['pets'][$oldName]);
		
		header("Location: ../pupper_page.php?name=".$name);
		exit();
	}
	
	//if name was not changed, redirect to pupper_page using old name
	header("Location: ../pupper_page.php?name=".$oldName);
	exit();
}
?>