<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("SELECT * FROM owner_pets WHERE username = ?");
$stmt->execute([$_SESSION['username']]);

//_SESSION array to keep track of the current users pets
$_SESSION['pets'] = array();

foreach($stmt as $pupper){
	//php _SESSION relational array stores pet_name as the key and pet_id as the value
	$_SESSION['pets'][$pupper['pet_name']] = $pupper['pet_id']; 
	echo"
	<tr>
		<input type=\"hidden\" value=\"".$pupper['pet_name']."\" id=\"petName\" name=\"petName\">
		<td><a href=\"pupper_page.php?name=".$pupper['pet_name']."\">".$pupper['pet_name']."<a></td>
		<td>".$pupper['pet_age']."</td>
		<td>".$pupper['breed_name']."</td>
		<td>".$pupper['pet_birthday']."</td>
		<td><button type=\"button\" class=\"btn btn-danger delete-pupper\">X</button></td>
	</tr>
	";
}
?>