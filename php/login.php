<?php
session_start();
require 'connect.php';

//If the POST var "login" exists (our submit button), then we can
//assume that the user has submitted the login form.
if(isset($_POST['login'])){
	
    //Retrieve the field values from our login form.
    $username = $_POST['username'];
    $password = $_POST['password'];
	
    //Retrieve the user account information for the given username and password
    $stmt = $pdo->prepare("SELECT * FROM OwnerLogin WHERE username = (?) AND password = (?)");
    $stmt->execute([$username, $password]);
    
    //Fetch row.
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    
    //If $row is FALSE.
    if(!$user){
        header("Location: ..\login_reg.php?error=x302");
		exit();
    } 
	else{            
	echo "login successful";
            //Provide the user with a login session.
            $_SESSION['username'] = $username;
            $_SESSION['userID'] = $user['owner_id'];
			
            header("Location: ..\user_account.php");
            exit();
	}
}else{echo"login is not set";}