<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("DELETE FROM PetVet WHERE pet_id = ? AND vet_id = ?");
$stmt->execute([$_SESSION['pets'][$_POST['petName']], $_SESSION['vets'][$_POST['vetName']]]);

//send back the number of records deleted
$status = $stmt->rowCount() > 0;

//send back a JSON
echo json_encode(array('status' => $status));
?>