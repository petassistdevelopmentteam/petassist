<?php
session_start();
require 'connect.php';

//If the POST var "register" exists (our submit button), then form data 
//was submitted
if(isset($_POST['register'])){
    //Retrieve the contraining username and email fields
    $username = $_POST['username'];
	$email = $_POST['email'];
    
    /*********** Checking if username or email exists ***********/
	
    //Construct the SQL statements and prepare them
    $name_stmt = $pdo->prepare("SELECT * FROM OwnerLogin WHERE username = ?");
    $name_stmt->execute([$username]);
    
	$email_stmt = $pdo->prepare("SELECT * FROM OwnerEmail WHERE email = ?");
    $email_stmt->execute([$email]);
    
	//Fetch the row.
    $name_row = $name_stmt->fetch(PDO::FETCH_ASSOC);
	$email_row = $email_stmt->fetch(PDO::FETCH_ASSOC);
    
    //If the provided username already exists - display error.
    if($email_row){
		header("Location: ..\login_reg.php?error=x546");
		exit();
	}
	else if($name_row){
        header("Location: ..\login_reg.php?error=x556");
		exit();
    }	
    else{
		/*********** Inserting new user ***********/
		
		//pull remaining values
		$fname = $_POST['firstName'];
		$lname = $_POST['lastName'];		
		$pass = $_POST['password']; 
		
		//Prepare our INSERT statements and execute
		//insert OwnerLogin and start a user session
		$stmt = $pdo->prepare("INSERT INTO OwnerLogin(username, password) VALUES (?, ?)");
		$stmt->execute([$username, $pass]);
		$_SESSION['username'] = $username;
		$_SESSION['userID'] = $pdo->lastInsertID();
		
		//insert OwnerName
		$stmt = $pdo->prepare("INSERT INTO OwnerName(owner_id, first_name, last_name) VALUES (?, ?, ?)");
		$stmt->execute([$_SESSION['userID'], $fname, $lname]);
		//insert OwnerEmail
		$stmt = $pdo->prepare("INSERT INTO OwnerEmail(owner_id, email) VALUES (?, ?)");
		$stmt->execute([$_SESSION['userID'], $email]);
		
		header("Location: ..\user_account.php");
		exit();
	}
}
?>