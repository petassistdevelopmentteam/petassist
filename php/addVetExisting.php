<?php
session_start();
require 'connect.php';

if(isset($_POST['addVet'])){
	$pet_id = $_SESSION['pets'][$_POST['petname']];

	$stmt = $pdo->prepare("INSERT INTO PETVET(pet_id, vet_id) VALUES (?,?)");
	$stmt->execute([$pet_id, $_SESSION['vets'][$_POST['vet']]]);
	
	header("Location: ../pupper_page.php?name=".$_POST['petname']);
	exit();
}
?>