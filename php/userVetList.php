<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("SELECT DISTINCT * FROM Owner_Vet WHERE username = ?");
$stmt->execute([$_SESSION['username']]);

//_SESSION array to keep track of the current users vets
$_SESSION['vets'] = array();

foreach($stmt as $vet){
	//server  side $_SESSION hashmap vets stores list of user vets
	// vet_name => vet_id
	//checks to make sure it isn't already stored
	if(!isset($_SESSION['vets'][$vet['vet_name']])){
		$_SESSION['vets'][$vet['vet_name']] = $vet['vet_id'];
	}
	echo"
	<tr>
		<input type=\"hidden\" value=\"".$vet['vet_name']."\" id=\"vetName\" name=\"vetName\">
		<td><a href=\"vet_page.php?name=".$vet['vet_name']."\">".$vet['vet_name']."<a></td>
		<td><button type=\"button\" class=\"btn btn-danger delete-vet\">X</button></td>
	</tr>
	";
}
?>