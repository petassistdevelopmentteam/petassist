<?php
session_start();
require 'connect.php';

//check if update_user is is set
if(isset($_POST['update_user'])){

    //Verify the entered password matches what's on record
    $password_stmt = $pdo->prepare("SELECT * FROM OwnerLogin WHERE owner_id = ?");
    $password_stmt->execute([$_SESSION['userID']]);
    $password_row = $password_stmt->fetch(PDO::FETCH_ASSOC);
    $current_pw = $password_row['password'];
    if($_POST['password'] == $current_pw){

        /*********** Checking if email exists ***********/
        $email_stmt = $pdo->prepare("SELECT * FROM OwnerEmail WHERE email = ?");
        $email_stmt->execute([$_POST['email']]);
        
        //Fetch the row.
        $email_row = $email_stmt->fetch(PDO::FETCH_ASSOC);
        
        //If the provided username already exists - display error.
        if($email_row){
            header("Location: ..\user_settings.php?error=x546");
            exit();
        }
        else{
            /*********** Updating the user's information user ***********/
            
            //Update ownerEmail
            if($_POST['email'] != null){
                $stmt = $pdo->prepare("UPDATE OwnerEmail SET email = ? WHERE owner_id = ?");
                $stmt->execute([$_POST['email'], $_SESSION['userID']]);
            }
            
            //Update OwnerName
            if(($_POST['firstName'] != null) && ($_POST['lastName'] != null)){
                $stmt = $pdo->prepare("UPDATE OwnerName SET first_name = ?, last_name = ? WHERE owner_id = ?");
                $stmt->execute([$_POST['firstName'], $_POST['lastName'], $_SESSION['userID']]);
            }
            //Updating first name only
            else if(($_POST['firstName'] != null) && ($_POST['lastName'] == null)){
                $stmt = $pdo->prepare("UPDATE OwnerName SET first_name = ? WHERE owner_id = ?");
                $stmt->execute([$_POST['firstName'], $_SESSION['userID']]);
            }
            //Updating last name only
            else if(($_POST['firstName'] == null) && ($_POST['lastName'] != null)){
                $stmt = $pdo->prepare("UPDATE OwnerName SET last_name = ? WHERE owner_id = ?");
                $stmt->execute([$_POST['lastName'], $_SESSION['userID']]);
            }

            //Update Password
            if($_POST['new-password'] != null && ($_POST['new-password'] != $current_pw)){
                $stmt = $pdo->prepare("UPDATE OwnerLogin SET password = ? WHERE owner_id = ?");
                $stmt->execute([$_POST['new-password'], $_SESSION['userID']]);
            }
            header("Location: ..\user_settings.php");
            exit();
        }
    }
}
?>