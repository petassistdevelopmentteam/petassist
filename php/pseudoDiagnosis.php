<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("SELECT scientific_name, common_name FROM DISEASE WHERE ? IN 
    (symptom_1, symptom_2, symptom_3, symptom_4, symptom_5, symptom_6)");
$stmt->execute([$_POST['symptom']]);

//send back the number of records deleted
$status = $stmt->rowCount() > 0;
$response = array();
$count = 0;
foreach($stmt as $disease){
   array_push($response, $disease['scientific_name']);
   $count++;
}
//send back a JSON
echo json_encode($response);
?>