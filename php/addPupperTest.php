<?php
session_start();
require 'connect.php';

if(isset($_POST['pupperKey']) && isset($_POST['diseaseKey'])){
		
	try{
	$stmt = $pdo->prepare("INSERT INTO PetTest(pet_id, scientific_name) VALUES (?,?)");
	$stmt->execute([$_SESSION['pets'][$_POST['pupperKey']], $_POST['diseaseKey']]);

	$status = $stmt->rowCount() > 0;
	}
	catch(Exception $e){
		$status = false;
	}
	echo json_encode($status);
}
?>
