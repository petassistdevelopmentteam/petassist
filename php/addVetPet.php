<?php
session_start();
require 'connect.php';

if(isset($_POST['addVet'])){
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$fax = $_POST['fax'];	
	$address = $_POST['address'];
	$pet_id = $_SESSION['pets'][$_POST['petname']];
		
	$stmt = $pdo->prepare("INSERT INTO Veterinarian(vet_name, vet_phone, vet_fax, vet_address) VALUES (?,?,?,?)");
	$stmt->execute([$name, $phone, $fax, $address]);

	$vet_id = $pdo->lastInsertID();
	
	$stmt = $pdo->prepare("INSERT INTO PetVet(pet_id, vet_id) VALUES (?,?)");
	$stmt->execute([$pet_id, $vet_id]);
	
	$stmt = $pdo->prepare("INSERT INTO OwnerVet(owner_id, vet_id) VALUES (?,?)");
	$stmt->execute([$_SESSION['userID'], $vet_id]);
	
	//adds the vet's name to the user 'vets' hashmap
	$_SESSION['vets'][$name] = $vet_id;
	
	header("Location: ../pupper_page.php?name=".$_POST['petname']);
	exit();
}
?>
