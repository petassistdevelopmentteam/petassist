<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("DELETE FROM PetInformation WHERE pet_id = ?");
$stmt->execute([$_SESSION['pets'][$_POST['petName']]]);

//send back the number of records deleted
$status = $stmt->rowCount() > 0;

//send back a JSON
echo json_encode(array('status' => $status));
?>