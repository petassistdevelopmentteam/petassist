<?php
session_start();
require 'connect.php';

$stmt = $pdo->prepare("DELETE FROM Veterinarian WHERE vet_id = ?");
$stmt->execute([$_SESSION['vets'][$_POST['vetName']]]);

//send back the number of records deleted
$status = $stmt->rowCount() > 0;

//send back a JSON
echo json_encode(array('status' => $status));
?>