 $(".nav-item").hover(function () {
    $(this).toggleClass("active");
 });
		
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}		

$(document).ready(function(){
	var date_input=$('input[name="birthday"]'); //our date input has the name "date"
	var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
	var options={
		format: 'yyyy/mm/dd',
		container: container,
		todayHighlight: true,
		autoclose: true,
	};
	date_input.datepicker(options);
});

$('.delete-pupper').click(function () {
    var button = $(this), 
        tr = button.closest('tr');
    // find the ID stored in the .input cell
    var name = tr.find('input').val();
    console.log('clicked button with pet key', name);

    // your PHP script expects pet_id so we need to pass it one
    var data = { petName: name };

    // ask confirmation
    if (confirm('Are you sure you want to delete this entry?')) {
        console.log('sending request');
        // delete record only once user has confirmed
        $.post('php\\deletePupper.php', data, function (res) {
            console.log('received response', res);
            // we want to delete the table row only we received a response back saying that it worked
            if (res.status) {
                console.log('deleting TR');
                tr.remove();
            }
        }, 'json');
    }
});

$('.remove-pupperVet').click(function () {
    var button = $(this), 
        tr = button.closest('tr');
    // find the ID stored in the .input cell
    var name = tr.find('input').val();
    console.log('clicked button with pet key', name);

	var vet = getUrlVars()["name"];
	try{
		console.log('pulled variable from url', decodeURI(vet));
		// your PHP script expects pet_id so we need to pass it one
		var data = { petName: name, vetName: decodeURI(vet) };
	}
	catch(e){console.log(e);}
    // ask confirmation
    if (confirm('Are you sure you want to delete this entry?')) {
        console.log('sending request');
        // delete record only once user has confirmed
        $.post('php\\deletePetVet.php', data, function (res) {
            console.log('received response', res);
            // we want to delete the table row only we received a response back saying that it worked
            if (res.status) {
                console.log('deleting TR');
                tr.remove();
            }
        }, 'json');
    }
});

$('.delete-vet').click(function () {
	console.log('Button clicked');
    var button = $(this), 
        tr = button.closest('tr');
    // find the ID stored in the .input cell
    var id = tr.find('input').val();
    console.log('clicked button with vet name', id);

    // your PHP script expects vet_id so we need to pass it one
    var data = { vetName: id };

    // ask confirmation
    if (confirm('Are you sure you want to delete this entry?')) {
        console.log('sending request');
        // delete record only once user has confirmed
        $.post('php\\deleteVet.php', data, function (res) {
            console.log('received response', res);
            // we want to delete the table row only we received a response back saying that it worked
            if (res.status) {
                console.log('deleting TR');
                tr.remove();
            }
        }, 'json');
    }
});

$('.delete-test').click(function () {
	console.log('Button clicked');
    var button = $(this), 
        tr = button.closest('tr');
    // find the ID stored in the .input cell
    var disease = tr.find('input').val();
    var pet = getUrlVars()["name"];
    console.log('clicked button with scientific_name', disease);

    // prep the data for our php deletion
    var data = { diseaseName : disease, petName : pet };

    // ask confirmation
    if (confirm('Are you sure you want to delete this entry?')) {
        console.log('sending request');
        // delete record only once user has confirmed
        $.post('php\\deletePetTest.php', data, function (res) {
            console.log('received response', res);
            // we want to delete the table row only we received a response back saying that it worked
            if (res.status) {
                console.log('deleting TR');
                tr.remove();
            }
        }, 'json');
    }
});

$('.pseudo_diagnosis').click(function () {
    console.log('Attempting diagnosis.');
    
    var test = []; //array to hold the possible test to run
    var testCount = 0; //running count of all the possible tests to ask vet
    //pulling the pressed buttons and storing them in the buttons array
    var buttons = document.querySelectorAll('[aria-pressed="true"]');
    for (var i = 0; i < buttons.length; i++) {
        var button = buttons[i];
        var data = { symptom : button.getAttribute('value')};
        console.log("Sending request for", button.getAttribute('value')+".");
        $.post('php\\pseudoDiagnosis.php', data, function (res) {
            console.log("Response from database:", res, "for", data.symptom);
            $('#symptom-list').empty(); //reset the table on a new diagnosis
            for(var i = 0; i < res.length; i++){
                if($.inArray(res[i], test) == -1){
                    console.log("Pushing:", res[i])
                    test.push(res[i]);
                    $('#symptom-list').append(
                        "<tr>"+
                            "<td>"+
                                "<a class=\"science-name\" value=\""+res[i]+"\" href=\"#\">"+res[i]+"</a>"+
                            "</td>"+
                            "<td>"+
                                "<a class=\"Description\" value=\""+res[i]+"\" href=\"#\">"+res[i]+"</a>"+
                            "</td>"+
                            "<td>"+
                                "<div class=\"input-group\">"+
                                    "<select class=\"custom-select pupper-list\"></select>"+
                                    "<div class=\"input-group-append\">"+
                                        "<button class=\"btn btn-outline-primary add-test\" type=\"button\">Add Test</button>"+
                                    "</div>"+
                                "</div>"+
                            "</td>"+
                        "</tr>"
                    );//end $('#symptom-list').append()
                }
                else{
                    console.log("Skipping:", res[i])
                }
            }
            return false;
        }, 'json')
        .done(function(){
            list_pets();
        })
        .fail(function(){
            console.log('Failed request for', data);
        });
    }// end for loop querying database for each button pressed
    
});

function list_pets(){
    $.post('php\\userPupperItemList.php', function (res) {
        var pupperLinks = "";
        for(var i = 0; i < res.length; i++){
            pupperLinks+="<option value=\""+res[i]+"\">"+res[i]+"</option>"
        }
        $('.pupper-list').html(pupperLinks);
    }, 'json');
}

$(document).on("click", ".add-test", function(){
    var tr = $(this).closest('tr');
        pupper = tr.find('select').val(),
        disease = tr.find('.science-name').text();
    console.log(pupper);
    console.log(disease);
    var data = {
        pupperKey : pupper,
        diseaseKey : disease
    };
    $.post("php\\addPupperTest.php", data, function(res){
        console.log(res);
        if(res){
            tr.find('.add-test').html("Added");
            tr.find('.add-test').removeClass('btn-outline-primary');
            tr.find('.add-test').removeClass('btn-outline-danger');
            tr.find('.add-test').addClass("btn-outline-success");
        }
        else{
            tr.find('.add-test').removeClass('btn-outline-primary');
            tr.find('.add-test').removeClass("btn-outline-success");
            tr.find('.add-test').addClass("btn-outline-danger");
            tr.find('.add-test').html("Already Added");
        }
    }, 'json');
});

$('.color-toggle').click(function(){
    $(this).toggleClass('btn-primary');
    $(this).toggleClass('bg-highlights');
});

$(document).ajaxComplete(function(){
    if($('#suggestion-table').length != 0) {
        $('#suggestion-table').css('text-align', 'center');
    }
});

$('#pupper-edit-button').click(function(){
    $('#pupper-info-form').toggle();
    $('#pupper-test-form').toggle();
    $('#pupper-update-form').toggle();
});

$('#vet-edit-button').click(function(){
    $('#vet-info-form').toggle();
    $('#appointment-form').toggle();
    $('#primary-physician-form').toggle();
    $('#vet-update-form').toggle();
});