<div class="jumbotron jumbotron-fluid clearfix">
	<div class="container">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Register</th>
					<th>Login</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form action="php\userRegistration.php" method="post">
							<div class="form-group form-row">
								<div class="col">
									<label for="firstName">First Name</label>
									<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First name" required>
								</div>
								<div class="col">
									<label for="lastName">Last Name</label>
									<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last name" required>	
								</div>
							</div>
							<div class="form-group">
								<label for="email">Email address
									<?php 
										if(isset($_GET['error']) && $_GET['error'] == 'x546'){
											echo "<span class=\"text-danger\"><b><small>Email in use.</small></b></span>"; 
											unset($_GET['error']);
										}?>
								</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
							</div>
							<div class="form-group">
								<label for="username">Username
									<?php 
										if(isset($_GET['error']) && $_GET['error'] == 'x556'){
											echo "<span class=\"text-danger\"><b><small>Username in use.</b></small></span>"; 
											unset($_GET['error']);
										}?>
										<span><small class="text-danger">&nbsp;This CANNOT be changed later.</small></span>
								</label>
								<input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
							</div>
							<small id="Help" class="form-text text-muted">We will <b>never</b> share your information with anyone else for any reason.</small>
							<button type="submit" name="register" class="btn btn-primary float-right">Register</button>
						</form>
					</td>
					<td>
						<form action="php\login.php" method="post">
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control" id="username" name="username" placeholder="Enter username" required>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
							</div>
							<?php 
								if(isset($_GET['error']) && $_GET['error'] == 'x302'){
									echo "<span class=\"text-danger\"><b><small>Incorrect username or password.</b></small></span>"; 
									unset($_GET['error']);
								}?>
							<button type="submit" name = "login" class="btn btn-primary float-right">Login</button>
						</form>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>