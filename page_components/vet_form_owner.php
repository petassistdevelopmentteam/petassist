<div class="jumbotron jumbotron-fluid clearfix">
	<div class="container">
		<h4>Add Vet</h4>
		<table class="table table-bordered information-table">
			<thead>
				<tr>
					<th class="text-center">Register a new Veterinarian</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form action="php\addVetOwner.php" method="post">
							<input type="hidden" name="petname" value="<?php echo $_GET['pet'] ?>"></input>
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Veterinarians's name" required>
							</div>
							<div class="form-group">
								<label for="phone">Phone</label>
								<input name="phone" class="form-control" id="phone" name="phone" placeholder="(123)456-7890" required>
							</div>
							<div class="form-group">
								<label for="fax">Fax</label>
								<input type="text" class="form-control" id="fax" name="fax" placeholder="Fax number" required>
							</div>
							<div class="form-group">
								<label for="adress">Address</label>
								<input type="text" class="form-control" id="address" name="address" placeholder="1234 Nowhere" required>
							</div>
							<button type="submit" name="addVet" class="btn btn-primary float-right">Add Vet</button>
						</form>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>