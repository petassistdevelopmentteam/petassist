<?php
session_start();
require 'php/connect.php';

$stmt = $pdo->prepare("SELECT * FROM Owner_Pets WHERE username = ? AND pet_name = ?");
$stmt->execute([$_SESSION['username'], $_GET['name']]);
$pupper = $stmt->fetch(PDO::FETCH_ASSOC);

$pupperID = $pupper['pet_id'];
$name = $pupper['pet_name'];
$age = $pupper['pet_age'];
$breed = $pupper['breed_name'];
$birthday = $pupper['pet_birthday'];

echo "
<div class=\"jumbotron jumbotron-fluid\">
  	<div class=\"container\">	
		<h1 class=\"display-4\"><b>".$name."'s Page</b></h1>
		<div class=\"container clearfix\">
			<table class=\"table table-bordered information-table\" style=\""; 
				if(isset($_GET['error'])){
					echo "display: none;";
				}
				echo "\" id=\"pupper-info-form\">
				<tbody>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Name: </th>
						<td>".$name."</td>
					</tr>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Age: </th>
						<td>".$age."</td>
					</tr>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Breed: </th>
						<td>".$breed."</td>
					</tr>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Birthday: </th>
						<td>".$birthday."</td>
					</tr>";
					$stmt = $pdo->prepare("SELECT vet_id, vet_name FROM Pet_Vet WHERE pet_id = ?");
					$stmt->execute([$pupperID]);
					if($vet=$stmt->fetch(PDO::FETCH_ASSOC)){
						echo"<tr>
								<th scope=\"row\" class=\"border border-secondary\">Vet: </th>
								<td><a href=\"vet_page.php?+&name=".$vet['vet_name']."\">".$vet['vet_name']."</a></td>
							</tr>";
					}
					else{
						echo"<tr>
								<th scope=\"row\" class=\"border border-secondary\">Vet: </th>
								<td><a href=\"add_vet.php?pet=".$name."\">New Vet</a></td>
							</tr>";
					}
				echo "
				</tbody>
				<button class=\"btn btn-primary btn-sm float-right\" id=\"pupper-edit-button\" type=\"button\">Edit</button>
				</table>
				<table class=\"table table-striped table-bordered information-table\"style=\""; 
					if(isset($_GET['error'])){
						echo "display: none;";
					}
					echo "\" id=\"pupper-test-form\">
					<thead>
						<tr>
							<th colspan=\"3\" class=\"border border-secondary text-center\">
								Pet Tests:
							</th>
						</tr>
					</thead>
					<tbody>";
					//Pull all test to associated pet
					$stmt = $pdo->prepare("SELECT * FROM PetTest WHERE pet_id = ?");
					$stmt->execute([$_SESSION['pets'][$_GET['name']]]);
					
					if($stmt->rowCount() > 0){
						foreach($stmt as $test){
							echo"
							<tr>
								<input type=\"hidden\" value=\"".$test['scientific_name']."\" id=\"scientific_name\" name=\"scientific_name\">
								<td><a href=\"pupper_page.php?name=".$test['scientific_name']."\">".$test['scientific_name']."<a></td>
								<td><button type=\"button\" class=\"btn btn-danger delete-test\">X</button></td>
							</tr>
							";
						}
					}
					else{
						echo"
							<tr>
								<td colspan=\"3\" class=\"border border-secondary text-center\">
									No tests yet.
								</td>
							</tr>";
					}
				echo "<tbody>
				</table>";
			include('pupper_update_form.php');
		echo "
		</div>
  	</div>
</div>"; ?>