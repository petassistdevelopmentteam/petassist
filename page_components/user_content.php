<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1 class="display-4"><?php echo $_SESSION['username'] ?>'s Puppers</h1>
		<div class="container clearfix">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="border border-secondary">Name</th>
						<th class="border border-secondary">Age</th>
						<th class="border border-secondary">Breed</th>
						<th class="border border-secondary">Birthday</th>
						<th class="border border-secondary">Remove Pupper</th>
					</tr>
				</thead>
				<tbody>
					<?php include("php\userPupperList.php"); ?>
				</tbody>
			</table>
			<a class="btn btn-primary btn-lg float-right" href="add_pupper.php" role="button">Add New Pupper</a>
		</div>
		<div class="container pt-3">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="border border-secondary">Name</th>
						<th class="border border-secondary">Remove Veterinarian</th>
					</tr>
				</thead>
				<tbody>
					<?php include("php\userVetList.php"); ?>
				</tbody>
			</table>
			<a class="btn btn-primary btn-lg float-right" href="add_Vet.php" role="button">Add New Vet</a>
		</div>
	</div>
</div>