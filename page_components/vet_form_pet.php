<div class="jumbotron jumbotron-fluid clearfix">
	<div class="container">
		<h4>Add Vet for <?php echo $_GET['pet']; ?></h4>
		<table class="table table-bordered information-table">
			<thead>
				<tr>
					<th class="text-center">Add an Existing Veterinarian</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center">
					<form action="php\addVetExisting.php" method="post">
						<input type="hidden" name="petname" value="<?php echo $_GET['pet'] ?>"></input>
						<?php
						require 'php/connect.php';
						$stmt = $pdo->prepare("SELECT * FROM Owner_Vet WHERE username = ?");
						$stmt->execute([$_SESSION['username']]);
						if($stmt->rowCount() > 0){
							echo"							
							<div class=\"form-group\">
								<select name=\"vet\" class=\"form-control\" id=\"vet\" name=\"vet\">";
								foreach($stmt as $vet){
									echo"<option value=\"".$vet['vet_name']."\" id=\"name\" name=\"name\">".$vet['vet_name']."</option>";
								}
								echo "
								</select>
							</div>
							<button type=\"submit\" name=\"addVet\" class=\"btn btn-primary float-right\">Add Vet</button>";
						}
						else{
							echo "No Existing vets yet.";
						}
						?>
					</form>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="table table-bordered information-table">
			<thead>
				<tr>
					<th class="text-center">Register a new Veterinarian</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form action="php\addVetPet.php" method="post">
							<input type="hidden" name="petname" value="<?php echo $_GET['pet'] ?>"></input>
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Veterinarians's name" required>
							</div>
							<div class="form-group">
								<label for="phone">Phone</label>
								<input name="phone" class="form-control" id="phone" name="phone" placeholder="(123)456-7890" required>
							</div>
							<div class="form-group">
								<label for="fax">Fax</label>
								<input type="text" class="form-control" id="fax" name="fax" placeholder="Fax number" required>
							</div>
							<div class="form-group">
								<label for="adress">Address</label>
								<input type="text" class="form-control" id="address" name="address" placeholder="1234 Nowhere" required>
							</div>
							<button type="submit" name="addVet" class="btn btn-primary float-right">Add Vet</button>
						</form>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>