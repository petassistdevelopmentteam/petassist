<?php
    echo "
    <table class=\"table table-bordered information-table\" style=\"display: none;\" id=\"vet-update-form\">
        <thead>
            <tr>
                <th class=\"text-center\">Update Form</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <form action=\"php\updateVet.php\" method=\"post\">
                    <input type=\"hidden\" class=\"form-control\" id=\"oldName\" name=\"oldName\" value=\"".$_GET['name']."\">
                        <div class=\"form-group\">
                            <label for=\"name\">Name</label>
                            <input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"".$name."\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"phone\">Phone</label>
                            <input name=\"phone\" class=\"form-control\" id=\"phone\" name=\"phone\" placeholder=\"".$phone."\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"fax\">Fax</label>
                            <input type=\"text\" class=\"form-control\" id=\"fax\" name=\"fax\" placeholder=\"".$fax."\">
                        </div>
                        <div class=\"form-group\">
                            <label for=\"adress\">Address</label>
                            <input type=\"text\" class=\"form-control\" id=\"address\" name=\"address\" placeholder=\"".$address."\">
                        </div>
                        <button type=\"submit\" name=\"addVet\" class=\"btn btn-primary float-right\">Update</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>";
?>