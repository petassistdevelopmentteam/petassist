<?php
    require 'php/connect.php';
    $stmt = $pdo->prepare("SELECT * FROM Owner_Information WHERE owner_ID = ?");
    $stmt->execute([$_SESSION['userID']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    echo "
    <div class=\"jumbotron jumbotron-fluid clearfix\">
        <div class=\"container\">
            <table class=\"table table-bordered information-table\">
                <thead>
                    <tr>
                        <th>Update Account Settings</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <form action=\"php\userUpdate.php\" id=\"user_update_form\" method=\"post\">
                                <div class=\"form-group form-row\">
                                    <div class=\"col\">
                                        <label for=\"firstName\">First Name</label>
                                        <input type=\"text\" class=\"form-control\" id=\"firstName\" name=\"firstName\" placeholder=\"".$user['first_name']."\">
                                    </div>
                                    <div class=\"col\">
                                        <label for=\"lastName\">Last Name</label>
                                        <input type=\"text\" class=\"form-control\" id=\"lastName\" name=\"lastName\" placeholder=\"".$user['last_name']."\">	
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"email\">Email";
                                            if(isset($_GET['error']) && $_GET['error'] == 'x546'){
                                                echo "<span class=\"text-danger\"><b><small>&nbsp;Email in use.</small></b></span>"; 
                                                unset($_GET['error']);
                                            }
                                    echo "
                                    </label>
                                    <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" placeholder=\"".$user['email']."\">
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"password\">New Password<span><small class=\"text-muted\">&nbsp;Required if changing password.</small></span></label>
                                    <input type=\"password\" class=\"form-control\" id=\"new-password\" name=\"new-password\" placeholder=\"New Password\">
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"password\">Password<span><small class=\"text-muted\">&nbsp;Required to update information.</small></span></label>
                                    <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" placeholder=\"Password\" required>
                                </div>
                                <button type=\"submit\" name=\"update_user\" class=\"btn btn-primary float-right\">Save</button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>";
?>