<?php session_start(); ?>
<!DOCTYPE html>
<html class="h-100" lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<!-- DatePicker CSS -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

		<!-- Self-authored CSS -->
		<link rel="stylesheet" href="styles.css" type="text/css">

		<!-- Font imports -->
		<link href="https://fonts.googleapis.com/css?family=Chivo:900" rel="stylesheet">
	</head>
	<body  class="h-100">
		<div class="container-fluid h-100">
			<div class="row h-15 bg-main">
				<div class="col">
					<h1><b>PetAssist</b></h1>
				</div>
			</div>
			<div class="row h-85">
				<div class="col main-col bg-highlights">
					<nav class="navbar navbar-expand-md navbar-dark bg-main" id="navbar-overlay">
						<button class="navbar-toggler mr-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNav">
							<ul class="navbar-nav">
								<li class="nav-item">
									<a class="nav-item nav-link text-center" href="index.php">Home</a>
								</li>
								<li class="nav-item">
									<a class="nav-item nav-link text-center" href="#">About</a>
								</li>
								<li class="nav-item">
									<a class="nav-item nav-link text-center" href="#">FAQs</a>
								</li>
								<li class="nav-item">
									<a class="nav-item nav-link text-center" href="#">Basic Training</a>
								</li>
								<li class="nav-item">
									<a class="nav-item nav-link text-center" href="#">Contact</a>
								</li>
							</ul>
						</div>
						<?php include ('page_components\user_utility_button.php'); ?>
					</nav>
					<main class="scroll-box">