<div class="jumbotron jumbotron-fluid clearfix">
	<div class="container">
		<h4>Pupper Registration</h4>
		<table class="table table-bordered information-table" id="registration-form">
			<thead>
				<tr>
					<th>Register</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form action="php\addPupper.php" method="post">
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Pupper's name" required>
							</div>
							<div class="form-group">
								<label for="breed">Breed</label>
								<select name="breed" class="form-control" id="breed" name="breed">
									<?php include('php\listBreed.php'); ?>
								</select>
							</div>
							<div class="form-group">
								<label for="age">Age</label>
								<input type="text" class="form-control" id="age" name="age" placeholder="Age" required>
							</div>
							<div class="form-group">
								<label for="birthday">Birthday</label>
								<?php 
									if(isset($_GET['error']) && $_GET['error'] == 'x234'){
										echo "<span class=\"text-danger\"><b><small>&nbsp;Invalid date format.</small></b></span>"; 
										unset($_GET['error']);
									}?>
								<input type="text" class="form-control" id="date" name="birthday" placeholder="YYYY/MM/DD" required>
							</div>
							<button type="submit" name="addPupper" class="btn btn-primary float-right">Register</button>
						</form>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>