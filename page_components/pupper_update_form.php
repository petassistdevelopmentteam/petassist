<?php
	echo "
	<table class=\"table table-bordered information-table\" style=\"";
		if(!isset($_GET['error'])){
			echo "display: none;";
		}
		echo "\" id=\"pupper-update-form\">
		<thead>
			<tr>
				<th>Update ".$_GET['name']."</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<form action=\"php\updatePupper.php\" method=\"post\">
						<div class=\"form-group\">
						<input type=\"hidden\" class=\"form-control\" id=\"oldName\" name=\"oldName\" value=\"".$_GET['name']."\">
							<label for=\"name\">Name</label>
							<input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"".$name."\">
						</div>
						<div class=\"form-group\">
							<label for=\"breed\">Breed</label>
							<select name=\"breed\" class=\"form-control\" id=\"breed\" name=\"breed\">
								<option value=\"\" hidden>".$breed."</option>";
								include('php\listBreed.php');
							echo "
							</select>
						</div>
						<div class=\"form-group\">
							<label for=\"age\">Age</label>
							<input type=\"text\" class=\"form-control\" id=\"age\" name=\"age\" placeholder=\"".$age."\">
						</div>
						<div class=\"form-group\">
							<label for=\"birthday\">Birthday</label>";
								if(isset($_GET['error']) && $_GET['error'] == 'x234'){
									echo "<span class=\"text-danger\"><b><small>&nbsp;Invalid date format.</small></b></span>";
									unset($_GET['error']);
								}
							echo "
							<input type=\"text\" class=\"form-control\" id=\"date\" name=\"birthday\" placeholder=\"".$birthday."\">
						</div>
						<button type=\"submit\" name=\"addPupper\" class=\"btn btn-primary float-right\">Update</button>
					</form>
				</td>
			</tr>
		</tbody>
	</table>";
?>