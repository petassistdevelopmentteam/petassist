<?php
if(isset($_SESSION['username'])){
	echo "
		<div class=\"btn-group ml-auto\">
			<a class=\"btn btn-sm btn-primary\" href=\"user_account.php\">".$_SESSION['username']."'s account</a>
			<a class=\"btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
				<span class=\"sr-only\">Toggle Dropdown</span>
			</a>
			<div class=\"dropdown-menu dropdown-menu-right\">
				<a class=\"dropdown-item\" href=\"user_account.php\">Profile home</a>
				<a class=\"dropdown-item\" href=\"test_suggester.php\">Test Suggester</a>
				<a class=\"dropdown-item\" href=\"user_settings.php\">Settings</a>
				<a class=\"dropdown-item\" href=\"php\logout.php\">Logout</a>
			</div>
		</div>";
}
else{
	echo "<a class=\"btn btn-sm btn-primary ml-auto\" name=\"login\" href=\"login_reg.php\">Login/Register</a>";
}
?>