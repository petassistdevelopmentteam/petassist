<?php
session_start();
require 'php/connect.php';

$stmt = $pdo->prepare("SELECT * FROM Veterinarian WHERE vet_id = ?");
$stmt->execute([$_SESSION['vets'][$_GET['name']]]);
$vet = $stmt->fetch(PDO::FETCH_ASSOC);

$vetID = $vet['vet_id'];
$name = $vet['vet_name']; //used as check to make sure correct pet was called
$phone = $vet['vet_phone'];
$fax = $vet['vet_fax'];
$address = $vet['vet_address'];

echo "
<div class=\"jumbotron jumbotron-fluid\">
	<div class=\"container\">
		<h1 class=\"display-4\"><b>".$name."</b></h1>";		
		//table for vet information
		echo "
		<div class=\"container clearfix\">
			<table class=\"table table-bordered information-table\" id=\"vet-info-form\">
				<tbody>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Name: </th>
						<td>".$name."</td>
					</tr>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Phone: </th>
						<td>".$phone."</td>
					</tr>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Fax: </th>
						<td>".$fax."</td>
					</tr>
					<tr>
						<th scope=\"row\" class=\"border border-secondary\">Address: </th>
						<td>".$address."</td>
					</tr>
					<button class=\"btn btn-primary btn-sm float-right\" id=\"vet-edit-button\" type=\"button\">Edit</button>
				</tbody>
			</table>";
			include('vet_update_form.php');
		echo "
		</div>";//end table vet info		
		//table Primary care physician
		echo "
		<div class=\"container clearfix\">
			<table class=\"table table-striped table-bordered information-table\" id=\"primary-physician-form\">
				<thead>
					<tr>
						<th colspan=\"3\" class=\"border border-secondary text-center\">
							Primary Care Physician For:
						</th>
					</tr>
				</thead>
				<tbody>";
			//Pull all pets associated to vet
			$stmt = $pdo->prepare("SELECT * FROM Pet_Vet WHERE vet_id = ?");
			//if there are any pets display them
			$stmt->execute([$vetID]);
			
			if($stmt->rowCount() > 0){
				foreach($stmt as $pet){
					echo"
					<tr>
						<input type=\"hidden\" value=\"".$pet['pet_name']."\" id=\"petname\" name=\"petname\">
						<td><a href=\"pupper_page.php?name=".$pet['pet_name']."\">
							".$pet['pet_name']."</a>
						</td>
						<td><button type=\"button\" class=\"btn btn-danger remove-pupperVet\">X</button></td>
					</tr>";
				}
			}
			//else no pets associated with the vet and nothing is displayed
			else{
					echo"
					<tr>
						<td colspan=\"3\" class=\"border border-secondary text-center\">
							No assigned pets yet.
						</td>
					</tr>";
			}
			echo "<tbody>
			</table>
		</div>"; //end table Primary care physician		
		//table upcoming appointments
		echo "
		<div class=\"container clearfix\">
			<table class=\"table table-striped table-bordered information-table\" id=\"appointment-form\">
				<thead>
					<tr>
						<th colspan=\"2\" class=\"text-center\">Upcoming Appointments</th>
					</tr>
				</thead>
				<tbody>";
				$stmt = $pdo->prepare("SELECT * FROM vet_appointment WHERE vet_id = ?");
				$stmt->execute([$vetID]);
				if($stmt->rowCount() > 0){
					echo "
					<tr>
						<td class=\"col-8\"></td>
						<td class=\"col-3\"></td>
					</tr>";
				}
				else{
					echo"
					<tr>
						<td colspan=\"2\" class=\"border border-secondary text-center\">
							No upcoming appointments.
						</td>
					</tr>";
				}				
				echo "	
				</tbody>
			</table>
		</div>"; //end table upcoming appointments		
	echo "
	</div>
</div>"; ?>