<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1 class="display-5"><?php echo $_SESSION['username'] ?> Test Suggestion Service</h1>
		<div class="container">
			<table id="symptom-table" class="table table-bordered">
				<thead>
					<tr>
						<th scope="col" colspan="4">Symptoms Table</th>
					</tr>
				</thead>
				<tbody>
					<tr scope="row">
						<td>
							<button class="btn btn-primary color-toggle" name="symptom1" data-toggle="button" aria-pressed="false" href="#" value="coughing" role="button">Coughing</button>
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom2" data-toggle="button" aria-pressed="false" href="#" value="lethargy" role="button">Lethargy</a>
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom3" data-toggle="button" aria-pressed="false" href="#" value="loss of appetite" role="button">Loss of Appetite</a>
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom4" data-toggle="button" aria-pressed="false" href="#" value="watery eyes" role="button">Watery Eyes</a>
						</td>
					</tr>
					<tr scope="row">
						<td>
							<button class="btn btn-primary color-toggle" name="symptom5" data-toggle="button" aria-pressed="false" href="#" value="vomiting" role="button">Vomiting</a>
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom6" data-toggle="button" aria-pressed="false" href="#" value="diarrhea" role="button">Diarrhea</a>	
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom7" data-toggle="button" aria-pressed="false" href="#" value="runny nose" role="button">Runny Nose</a>
						</td>
						<td>
						</td>
					</tr>
					<tr scope="row">
						<td>
							<button class="btn btn-primary color-toggle" name="symptom8" data-toggle="button" aria-pressed="false" href="#" value="anorexia" role="button">Anorexia</a>
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom9" data-toggle="button" aria-pressed="false" href="#" value="big belly" role="button">Big Belly</a>
						</td>
						<td>
							<button class="btn btn-primary color-toggle" name="symptom10" data-toggle="button" aria-pressed="false" href="#" value="fever" role="button">Fever</a>
						</td>
						<td></td>
					</tr>
					<tr scope="row">
						<td colspan="4">
							<button class="btn btn-primary btn-lg mx-auto pseudo_diagnosis" role="button">Submit</a>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="container">
				<table class="table table-striped table-bordered" id="suggestion-table">
					<thead>
						<th scope="col" class="border border-secondary">Scientific Name</th>
						<th scope="col" class="border border-secondary">Description</th>
						<th scope="col" class="border border-secondary">Add to Pet</th>
					</thead>
					<tbody id="symptom-list"></tbody>
				</table>
			</div>
		</div>
	</div>
</div>